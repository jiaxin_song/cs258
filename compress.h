#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#ifndef _COMPRESS_H_
#define _COMPRESS_H_

void encode(int h, int w, void *_data, const char *path);
void decode(const char *path, int &h, int &w, void *_data);

#endif
