#include "compress.h"

static unsigned char div(unsigned char n) { return (n + 5) / 10; }

void encode(int h, int w, void *_data, const char *path) {
    unsigned char data[h][w][3];
    memcpy(data, _data, sizeof(data));
    unsigned char ave[h][w];

    // TODO begin
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            /*
            ave[i][j] = (data[i][j][0] + data[i][j][1] + data[i][j][2]) / 3;
            int temp = ave[i][j];
            if (abs(data[i][j][0] - temp) < 20 && abs(data[i][j][1] - temp) < 20
            && abs(data[i][j][2] - temp) < 20) { data[i][j][0] = (unsigned
            char)256; data[i][j][1] = (unsigned char)div(temp);
            */
        }
    }

    auto f = fopen(path, "w");
    fprintf(f, "%d %d\n", h, w);
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++) {
            if (data[i][j][0] == (unsigned char)3) {
                fprintf(f, "%d ", data[i][j][0]);
                fprintf(f, "%d ", data[i][j][1]);
            } else {
                for (int k = 0; k < 3; k++)
                    fprintf(f, "%d ", data[i][j][k]);
            }
        }
    fclose(f);
    // end
}

void decode(const char *path, int &h, int &w, void *_data) {
    // TODO begin

    auto f = fopen(path, "r");
    fscanf(f, "%d%d", &h, &w);
    unsigned char data[h][w][3];

    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++)
            for (int k = 0; k < 3; ++k) {
                int number, x;
                fscanf(f, "%d", &number);
                if (number > 3) {
                    x = number * 10;
                    data[i][j][k] = (unsigned char)x;
                } else {
                    fscanf(f, "%d", &x);
                    x *= 10;
                    for (int l = 0; l < -number; ++l) {
                        data[i][j][l + k] = (unsigned char)x;
                    }
                    k = k - number - 1;
                }
            }
    fclose(f);
    // end
    memcpy(_data, data, sizeof(data));
}
