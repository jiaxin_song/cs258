main: judge.o compress.o
		g++ -o main judge.o compress.o

judge.o: judge.cpp
		g++ -c judge.cpp

compress.o: compress.cpp compress.h
		g++ -c compress.cpp

clean:
	rm main judge.o compress.o